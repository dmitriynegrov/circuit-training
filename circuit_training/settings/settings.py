from dataclasses import dataclass
from dataclasses_json import DataClassJsonMixin
from os import environ

@dataclass
class CostSettings(DataClassJsonMixin):
    wirelength: float
    congestion: float
    density: float

@dataclass
class PlacementSettings(DataClassJsonMixin):
    macro_min_spacing_x: float
    macro_min_spacing_y: float
    grid_cols: int
    grid_rows: int

@dataclass
class PhysicalSettings(DataClassJsonMixin):
    routes_hor: float
    routes_vert: float
    macro_routes_util_hor: float
    macro_routes_util_vert: float

@dataclass
class GroupingSettings(DataClassJsonMixin):
    legalize: bool

@dataclass
class Settings(DataClassJsonMixin):
    cost_function: CostSettings
    placement: PlacementSettings
    physical: PhysicalSettings
    grouping: GroupingSettings


if 'NFP_GLOBAL_SETTINGS' not in environ:
    exit("env NFP_GLOBAL_SETTINGS undefined")

with open(environ['NFP_GLOBAL_SETTINGS']) as f:
    SETTINGS: Settings
    SETTINGS = Settings.from_json(f.read())